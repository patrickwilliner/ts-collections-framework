import Stack from '../src/stack';
import { expect } from 'chai';

describe('push() function', () => {
  it('should push elements on the stack', () => {
    expect(new Stack().push(6).list.toArray()).to.deep.equal([6]);
    expect(new Stack().push(12).push(777).list.toArray()).to.deep.equal([12, 777]);
    expect(new Stack().push('a').push('b').push('c').list.toArray()).to.deep.equal(['a', 'b', 'c']);
  });
});

describe('pop() function', () => {
  it('should pop elements off the stack', () => {
    let stack = new Stack().push(6);
    expect(stack.pop()).to.deep.equal(6);
    expect(stack.list.toArray()).to.deep.equal([]);

    stack = new Stack().push('be').push('cool');
    expect(stack.pop()).to.deep.equal('cool');
    expect(stack.list.toArray()).to.deep.equal(['be']);
    expect(stack.pop()).to.deep.equal('be');
    expect(stack.list.toArray()).to.deep.equal([]);

    stack = new Stack().push('around').push('the').push('world');
    expect(stack.pop()).to.deep.equal('world');
    expect(stack.list.toArray()).to.deep.equal(['around', 'the']);
    expect(stack.pop()).to.deep.equal('the');
    expect(stack.list.toArray()).to.deep.equal(['around']);
    expect(stack.pop()).to.deep.equal('around');
    expect(stack.list.toArray()).to.deep.equal([]);

    stack = new Stack().push(999);
    stack.pop();
    stack.push(111).push(222);
    expect(stack.list.toArray()).to.deep.equal([111, 222]);
    expect(stack.pop()).to.deep.equal(222);
    expect(stack.list.toArray()).to.deep.equal([111]);
  });
});