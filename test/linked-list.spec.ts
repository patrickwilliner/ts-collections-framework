import LinkedList from '../src/linked-list';
import { expect } from 'chai';

describe('getFirst() function', () => {
  it('should return first element', () => {
    expect(new LinkedList<number>().getFirst()).to.deep.equal(undefined);
    expect(new LinkedList<number>().add(65).getFirst()).to.deep.equal(65);
    expect(new LinkedList<number>().add(-99).add(12).getFirst()).to.deep.equal(-99);
  });
});

describe('getLast() function', () => {
  it('should return first element', () => {
    expect(new LinkedList<number>().getLast()).to.deep.equal(undefined);
    expect(new LinkedList<number>().add(5).getLast()).to.deep.equal(5);
    expect(new LinkedList<number>().add(6439).add(134).getLast()).to.deep.equal(134);
  });
});

describe('get(n) function', () => {
  it('should return n-th element', () => {
    let list = new LinkedList<string>();
    expect(list.get(0)).to.deep.equal(undefined);

    list.add('first');
    expect(list.get(0)).to.deep.equal('first');
    expect(list.get(1)).to.deep.equal(undefined);

    list.add('second');
    expect(list.get(0)).to.deep.equal('first');
    expect(list.get(1)).to.deep.equal('second');
    expect(list.get(2)).to.deep.equal(undefined);

    list.add('third');
    expect(list.get(0)).to.deep.equal('first');
    expect(list.get(1)).to.deep.equal('second');
    expect(list.get(2)).to.deep.equal('third');
    expect(list.get(3)).to.deep.equal(undefined);

    list.add('fourth');
    expect(list.get(0)).to.deep.equal('first');
    expect(list.get(1)).to.deep.equal('second');
    expect(list.get(2)).to.deep.equal('third');
    expect(list.get(3)).to.deep.equal('fourth');
    expect(list.get(4)).to.deep.equal(undefined);
  });
});

describe('iterator', () => {
  it('should iterate', () => {
    let collected;

    collected = [];
    for (let elem of new LinkedList<number>()) {
      collected.push(elem);
    }
    expect(collected).to.deep.equal([]);

    collected = [];
    for (let elem of new LinkedList<number>().add(1)) {
      collected.push(elem);
    }
    expect(collected).to.deep.equal([1]);

    collected = [];
    for (let elem of new LinkedList<number>().add(1).add(-15)) {
      collected.push(elem);
    }
    expect(collected).to.deep.equal([1, -15]);

    collected = [];
    for (let elem of new LinkedList<string>().add('look').add(' @ ').add('me!')) {
      collected.push(elem);
    }
    expect(collected).to.deep.equal(['look', ' @ ', 'me!']);
  });
});

describe('add() function', () => {
  it('should add element', () => {
    expect(new LinkedList().add(6).toArray()).to.deep.equal([6]);
    expect(new LinkedList().add(6).add(6).toArray()).to.deep.equal([6, 6]);
    expect(new LinkedList().add('\n').add('this').toArray()).to.deep.equal(['\n', 'this']);
    expect(new LinkedList().add(true).add(false).toArray()).to.deep.equal([true, false]);
    expect(new LinkedList().add(Number.NaN).toArray()).to.deep.equal([Number.NaN]);
    expect(new LinkedList().add({}).toArray()).to.deep.equal([{}]);
    expect(new LinkedList().add(null).toArray()).to.deep.equal([null]);
    expect(new LinkedList().add(undefined).toArray()).to.deep.equal([undefined]);
    expect(new LinkedList().add(0).toArray()).to.deep.equal([0]);
    expect(new LinkedList().add('').toArray()).to.deep.equal(['']);
    expect(new LinkedList().add(false).toArray()).to.deep.equal([false]);
  });
});

describe('removeFirst() function', () => {
  it('should remove first element', () => {
    let list: LinkedList<number>;

    list = new LinkedList(849);
    expect(list.removeFirst()).to.deep.equal(849);
    expect(list.toArray()).to.deep.equal([]);

    list = new LinkedList(-457, 9346);
    expect(list.removeFirst()).to.deep.equal(-457);
    expect(list.toArray()).to.deep.equal([9346]);
    expect(list.removeFirst()).to.deep.equal(9346);
    expect(list.toArray()).to.deep.equal([]);

    list = new LinkedList(5, 4, 3);
    expect(list.removeFirst()).to.deep.equal(5);
    expect(list.toArray()).to.deep.equal([4, 3]);
    expect(list.removeFirst()).to.deep.equal(4);
    expect(list.toArray()).to.deep.equal([3]);
    expect(list.removeFirst()).to.deep.equal(3);
    expect(list.toArray()).to.deep.equal([]);
  });
});

describe('removeLast() function', () => {
  it('should remove last element', () => {
    let list: LinkedList<string>;

    list = new LinkedList('lol');
    expect(list.removeLast()).to.deep.equal('lol');
    expect(list.toArray()).to.deep.equal([]);

    list = new LinkedList('bear', 'mouse');
    expect(list.removeLast()).to.deep.equal('mouse');
    expect(list.toArray()).to.deep.equal(['bear']);
    expect(list.removeLast()).to.deep.equal('bear');
    expect(list.toArray()).to.deep.equal([]);

    list = new LinkedList('kiwi', 'orange', 'pear');
    expect(list.removeLast()).to.deep.equal('pear');
    expect(list.toArray()).to.deep.equal(['kiwi', 'orange']);
    expect(list.removeLast()).to.deep.equal('orange');
    expect(list.toArray()).to.deep.equal(['kiwi']);
    expect(list.removeLast()).to.deep.equal('kiwi');
    expect(list.toArray()).to.deep.equal([]);

    list = new LinkedList();
    expect(list.add('1').toArray()).to.deep.equal(['1']);
    expect(list.removeLast()).to.deep.equal('1');
    expect(list.add('2').toArray()).to.deep.equal(['2']);

    list = new LinkedList();
    expect(list.add('1').toArray()).to.deep.equal(['1']);
    expect(list.removeFirst()).to.deep.equal('1');
    expect(list.add('2').toArray()).to.deep.equal(['2']);
  });
});

describe('remove() function', () => {
  it('should remove n-th element', () => {
    expect(new LinkedList(10001).remove(0).toArray()).to.deep.equal([]);
    expect(new LinkedList('1', '2').remove(0).toArray()).to.deep.equal(['2']);
    expect(new LinkedList('1', '2').remove(1).toArray()).to.deep.equal(['1']);
    expect(new LinkedList('1', '2', '3').remove(0).toArray()).to.deep.equal(['2', '3']);
    expect(new LinkedList('1', '2', '3').remove(1).toArray()).to.deep.equal(['1', '3']);
    expect(new LinkedList('1', '2', '3').remove(2).toArray()).to.deep.equal(['1', '2']);

    let list = new LinkedList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0);
    expect(list.remove(9).toArray()).to.deep.equal([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    expect(list.remove(0).toArray()).to.deep.equal([2, 3, 4, 5, 6, 7, 8, 9]);
    expect(list.remove(3).toArray()).to.deep.equal([2, 3, 4, 6, 7, 8, 9]);
    expect(list.remove(0).toArray()).to.deep.equal([3, 4, 6, 7, 8, 9]);
    expect(list.remove(5).toArray()).to.deep.equal([3, 4, 6, 7, 8]);
    expect(list.remove(2).toArray()).to.deep.equal([3, 4, 7, 8]);
    expect(list.remove(2).toArray()).to.deep.equal([3, 4, 8]);
    expect(list.remove(0).toArray()).to.deep.equal([4, 8]);
    expect(list.remove(0).toArray()).to.deep.equal([8]);
    expect(list.remove(0).toArray()).to.deep.equal([]);
  });
});

describe('clear() function', () => {
  it('should remove all elements', () => {
    expect(new LinkedList().clear().toArray()).to.deep.equal([]);
    expect(new LinkedList(66).clear().toArray()).to.deep.equal([]);
    expect(new LinkedList('', '').clear().toArray()).to.deep.equal([]);
    expect(new LinkedList('\n', '\n', '\n').clear().toArray()).to.deep.equal([]);
  });
});

describe('toArray() function', () => {
  it('should return an array that contains all elements', () => {
    expect(new LinkedList().toArray()).to.deep.equal([]);
    expect(new LinkedList(6).toArray()).to.deep.equal([6]);
    expect(new LinkedList(7, 13).toArray()).to.deep.equal([7, 13]);
    expect(new LinkedList(77, 3, -100).toArray()).to.deep.equal([77, 3, -100]);
  });
});

describe('length', () => {
  it('should return the number of element', () => {
    expect(new LinkedList().length).to.deep.equal(0);
    expect(new LinkedList('1').length).to.deep.equal(1);
    expect(new LinkedList('1', '2').length).to.deep.equal(2);
    expect(new LinkedList('1', '2', '3').length).to.deep.equal(3);
  });
});