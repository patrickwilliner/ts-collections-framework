import Hashtable from '../src/hashtable';
import { hashString } from '../src/hashtable-helper';
import { expect } from 'chai';

function stringEquals(s1: string, s2: string): boolean {
  return s1 === s2;
}

function numberEquals(n1: number, n2: number): boolean {
  return n1 === n2;
}

function stringCollisionHash(s: string): number {
  return 7;
}

describe('add() function', () => {
  it('should add elements to the hashtable', () => {
    let hashtable = new Hashtable(hashString, stringEquals);
    hashtable.add('first', { value: 'first' });
    expect(hashtable.get('first')).to.deep.equal({ value: 'first' });

    hashtable.add('second', { value: 'second' });
    expect(hashtable.get('first')).to.deep.equal({ value: 'first' });
    expect(hashtable.get('second')).to.deep.equal({ value: 'second' });
  });

  it('should add elements with hash collision', () => {
    let hashtable = new Hashtable(stringCollisionHash, stringEquals);
    hashtable.add('first', { value: 'first' });
    expect(hashtable.get('first')).to.deep.equal({ value: 'first' });

    hashtable.add('second', { value: 'second' });

    expect(hashtable.get('first')).to.deep.equal({ value: 'first' });
    expect(hashtable.get('second')).to.deep.equal({ value: 'second' });
  });
});

describe('remove() function', () => {
  it('should remove elements from hashtable', () => {
    let hashtable = new Hashtable(hashString, stringEquals);
    hashtable.add('first', { value: 'first' });
    expect(hashtable.get('first')).to.deep.equal({ value: 'first' });
    hashtable.remove('first');
    expect(hashtable.get('first')).to.equal(undefined);

    hashtable.add('first', { value: 'first' });
    hashtable.add('second', { value: 'second' });
    expect(hashtable.get('first')).to.deep.equal({ value: 'first' });
    expect(hashtable.get('second')).to.deep.equal({ value: 'second' });
    hashtable.remove('first');
    expect(hashtable.get('first')).to.equal(undefined);
    expect(hashtable.get('second')).to.deep.equal({ value: 'second' });
    hashtable.remove('second');
    expect(hashtable.get('first')).to.equal(undefined);
    expect(hashtable.get('second')).to.equal(undefined);
  });
});