import LinkedList from './linked-list';
import Collection from './collection';

export default class Stack<T> extends Collection<T> implements Iterable<T> {
  private _list = new LinkedList<T>();

  constructor() {
    super();
  }

  push(value: T): Stack<T> {
    this._list.add(value);
    return this;
  }

  pop(): T {
    return this._list.removeLast();
  }

  peek(): T {
    return this._list.getLast();
  }

  [Symbol.iterator](): Iterator<T> {
    return new StackIterator(this);
  }

  get length(): number {
    return this._list.length;
  }

  get list(): LinkedList<T> {
    return this._list;
  }
}

class StackIterator<T> implements Iterator<T> {
  constructor(private _stack: Stack<T>) {
  }

  next(): IteratorResult<T> {
    return {
      done: this._stack.isEmpty(),
      value: this._stack.pop()
    };
  }
}