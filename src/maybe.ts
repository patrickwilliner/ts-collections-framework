export default class Maybe<T> {
  static Nothing = new Maybe(undefined);

  private constructor(private value: T) {
  }

  isNothing(): boolean {
    return this === Maybe.Nothing;
  }

  map(fn: (value: T) => T): Maybe<T> {
    if (this.isNothing()) {
      return Maybe.Nothing;
    } else {
      return Maybe.just(fn(this.value));
    }
  }

  flatMap(fn: (value: T) => Maybe<T>): Maybe<T> {
    if (this.isNothing()) {
      return Maybe.Nothing;
    } else {
      return fn(this.value);
    }
  }

  static just<T>(value: T): Maybe<T> {
    if (value == null) {
      throw 'value must not be <null>|<undefined>';
    } else {
      return new Maybe(value);
    }
  }

  static of<T>(value: T): Maybe<T> {
    if (value == null) {
      return Maybe.Nothing;
    } else {
      return Maybe.just(value);
    }
  }
}