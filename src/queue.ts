import Collection from '../src/collection';
import LinkedList from '../src/linked-list';

export class Queue<T> extends Collection<T> implements Iterable<T> {
  private _list = new LinkedList<T>();

  constructor() {
    super();
  }

  enqueue(value: T): Queue<T> {
    this._list.add(value);
    return this;
  }

  dequeue(): T {
    return this._list.removeFirst();
  }

  peek(): T {
    return this._list.getFirst();
  }

  [Symbol.iterator](): Iterator<T> {
    return new QueueIterator(this);
  }

  get length(): number {
    return this._list.length;
  }

  get list(): LinkedList<T> {
    return this._list;
  }
}

class QueueIterator<T> implements Iterator<T> {
  constructor(private _queue: Queue<T>) {
  }

  next(): IteratorResult<T> {
    return {
      done: this._queue.isEmpty(),
      value: this._queue.dequeue()
    };
  }
}