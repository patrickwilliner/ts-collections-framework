import Collection from './collection';

export default class LinkedList<T> extends Collection<T> implements Iterable<T> {
  private _head: Node<T>;
  private _tail: Node<T>;
  private _length = 0;

  constructor(...initialValues: T[]) {
    super();
    for (let value of initialValues) {
      this.add(value);
    }
  }

  getFirst(): T {
    return this._head?.value;
  }

  getLast(): T {
    return this._tail?.value;
  }

  get(index: number): T | undefined {
    if (index < 0 || index >= this.length) {
      return undefined;
    } else if (index < this.length / 2) {
      return this.getLeft(index);
    } else {
      return this.getRight(index);
    }
  }

  [Symbol.iterator](): Iterator<T> {
    return new LinkedListIterator(this._head);
  }

  add(value: T): LinkedList<T> {
    if (this._head == null) {
      this._head = this._tail = { value: value };
    } else {
      this._tail.next = { value: value, previous: this._tail };
      this._tail = this._tail.next;
    }
    this._length++;
    return this;
  }

  removeFirst(): T {
    if (this._head == null) {
      throw 'cannot remove from empty list';
    } else {
      let node = this._head;
      if (this._length === 1) {
        delete this._head;
        delete this._tail;
      } else {
        delete this._head.next.previous;
        this._head = node.next;
      }
      this._length--;
      return node.value;
    }
  }

  removeLast(): T {
    if (this._tail == null) {
      throw 'cannot remove from empty list';
    } else {
      let node = this._tail;
      if (this._length === 1) {
        delete this._head;
        delete this._tail;
      } else {
        delete this._tail.previous.next;
        this._tail = node.previous;
      }
      this._length--;
      return node.value;
    }
  }

  remove(index: number): LinkedList<T> {
    if (index < 0 || index >= this._length) {
      throw `index out of bounds: ${index}`;
    } else {
      let pointer = this._head;
      for (let i = 0; i < index; i++) {
        pointer = pointer.next;
      }

      if (pointer.previous == null) {
        this.removeFirst();
      } else if (pointer.next == null) {
        this.removeLast();
      } else {
        pointer.next.previous = pointer.previous;
        pointer.previous.next = pointer.next;
        this._length--;
      }

      return this;
    }
  }

  clear(): LinkedList<T> {
    delete this._head;
    delete this._tail;
    this._length = 0;
    return this;
  }

  toArray(): T[] {
    let a = new Array<T>(this._length);

    let pointer = this._head;
    for (let i = 0; i < this._length; i++) {
      a[i] = pointer.value;
      pointer = pointer.next;
    }

    return a;
  }

  get length(): number {
    return this._length;
  }

  private getLeft(index: number): T {
    let pointer = this._head;
    for (let i = 0; i < index; i++) {
      pointer = pointer.next;
    }
    return pointer?.value;
  }

  private getRight(index: number): T {
    let pointer = this._tail;
    for (let i = this._length - index - 1; i > 0; i--) {
      pointer = pointer.previous;
    }
    return pointer?.value;
  }
}

interface Node<T> {
  value: T,
  previous?: Node<T>,
  next?: Node<T>
}

class LinkedListIterator<T> implements Iterator<T> {
  constructor(private _current: Node<T>) {
  }

  next(): IteratorResult<T> {
    let result = {
      done: this._current == null,
      value: this._current?.value
    };
    this._current = this._current?.next;
    return result;
  }
}