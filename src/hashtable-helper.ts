export function normalizeHash(hash: number, hashtableCapacity: number): number {
  return Math.abs(hash % hashtableCapacity);
}

// djb2
export function hashString(s: string): number {
  let hash = 7;

  for (let i = 0; i < s.length; i++) {
    hash = hash*31 + s.charCodeAt(i);
  }

  return hash;
}