export default abstract class Collection<T> {
  abstract length: number;

  isEmpty(): boolean {
    return this.length === 0;
  }
}