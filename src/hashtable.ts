import Collection from './collection';
import { normalizeHash } from './hashtable-helper';

const DEFAULT_CAPACITY = 11;
const DEFAULT_LOADFACTOR = 0.75;
const TOMBSTONE: Tombstone = {}

export default class Hashtable<K, V> extends Collection<V> {
  private _hashtable: { keys: K | Tombstone[], values: V[] };
  private _length: number;
  private _usedBuckets: number;
  private _threshold: number;

  constructor(
    private _hashCodeFn: (key: K) => number,
    private _equalsFn: (key1: K, key2: K) => boolean,
    private _capacity = DEFAULT_CAPACITY,
    private _loadFactor = DEFAULT_LOADFACTOR) {
    super();

    this._hashtable = {
      keys: new Array(this._capacity),
      values: new Array(this._capacity)
    };
    this._length = this._usedBuckets = 0;
  }

  add(key: K, value: V): void {
    if (this.needsRehash()) {
      this.rehash();
    }

    let bucket = this.findBucket(key);

    if (this._hashtable.keys[bucket.index] == null) {
      // bucket is empty

      let index = bucket.tombstone === -1 ? bucket.index : bucket.tombstone;
      if (bucket.tombstone !== -1) {
        this._usedBuckets++;
      }

      this._hashtable.keys[index] = key;
      this._hashtable.values[index] = value;
      this._length++;
    } else {
      // replace old entry
      if (bucket.tombstone === -1) {
        this._hashtable.values[bucket.index] = value;
      } else {
        this._hashtable.keys[bucket.index] = TOMBSTONE;
        delete this._hashtable.values[bucket.index];
        this._hashtable.keys[bucket.tombstone] = key;
        this._hashtable.values[bucket.tombstone] = value;
      }
    }
  }

  remove(key: K): V | undefined {
    let bucket = this.findBucket(key);

    if (this._hashtable.keys[bucket.index] == null) {
      return undefined;
    } else {
      this._length--;
      let oldValue = this._hashtable.values[bucket.index];
      this._hashtable.keys[bucket.index] = TOMBSTONE;
      delete this._hashtable.values[bucket.index];
      return oldValue;
    }
  }

  get(key: K): V {
    let bucket = this.findBucket(key);

    if (this._hashtable.keys[bucket.index] == null) {
      return undefined;
    } else {
      if (bucket.tombstone === -1) {
        return this._hashtable.values[bucket.index];
      } else {
        // Swap key-values pairs at indexes i and j.
        this._hashtable.keys[bucket.tombstone] = this._hashtable.keys[bucket.index];
        this._hashtable.values[bucket.tombstone] = this._hashtable.values[bucket.index];
        this._hashtable.keys[bucket.index] = TOMBSTONE;
        delete this._hashtable.values[bucket.index];
        return this._hashtable.values[bucket.tombstone];
      }
    }
  }

  get length(): number {
    return this._length;
  }

  private findBucket(key: K): { index: number, tombstone: number } {
    let index = 0;
    let j = -1;
    let x = 1;
    let offset = 0;

    while (true) {
      index = this.hashToIndex(this._hashCodeFn(key) + offset);

      if (this._hashtable.keys[index] == null) {
        // bucket is empty
        return { index: index, tombstone: j };
      } else if (this._hashtable.keys[index] === TOMBSTONE) {
        // bucket contains deletion marker
        if (j === -1) {
          j = index;
        }
      } else {
        // bucket contains key
        if (this._equalsFn(this._hashtable.keys[index], key)) {
          return { index: index, tombstone: j };
        }
      }

      offset += this.probeLinear(x++);
    }
  }

  private needsRehash(): boolean {
    return this._usedBuckets >= this._loadFactor;
  }

  private rehash(): void {
  }

  private hashToIndex(hash: number): number {
    return normalizeHash(hash, this._capacity);
  }

  private probeLinear(x: number): number {
    return x + 1;
  }
}

type Tombstone = {}